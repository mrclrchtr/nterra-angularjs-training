'use strict';

var gulp = require('gulp'),
    gutil = require('gulp-util'),
    del = require('del'),
    browserify = require('browserify'),
    watchify = require('watchify'),
    babelify = require('babelify'),
    source = require('vinyl-source-stream'),
    buffer = require('vinyl-buffer'),
    sourcemaps = require('gulp-sourcemaps'),
    sass = require('gulp-sass'),
    browserSync = require('browser-sync').create(),
    url = require('url'),
    proxy = require('proxy-middleware'),
    modRewrite = require('connect-modrewrite');

var requireDir = require('require-dir');

// import all tasks
requireDir('./gulp/tasks', {recurse: true});
