# Ernest Admin GUI

## Prerequirements

### Windows:
* Install Git https://git-scm.com/
* Install Microsoft Visual C++ Build Tools 2015 Technical Preview http://www.microsoft.com/en-us/download/details.aspx?id=49983
* Install node 5.2.0 https://nodejs.org/en/
* Others:
    * Option 1: Install all the required tools and configurations using Microsoft's [windows-build-tools](https://github.com/felixrieseberg/windows-build-tools) using `npm install --global --production windows-build-tools` from an elevated PowerShell or CMD.exe (run as Administrator).
    * Option 2: Install tools and configuration manually:
      * Visual C++ Build Environment:
        * Option 1: Install [Visual C++ Build Tools](http://landinghub.visualstudio.com/visual-cpp-build-tools) using the **Default Install** option.

        * Option 2: Install [Visual Studio 2015](https://www.visualstudio.com/products/visual-studio-community-vs) (or modify an existing installation) and select *Common Tools for Visual C++* during setup. This also works with the free Community and Express for Desktop editions.

        > :bulb: [Windows Vista / 7 only] requires [.NET Framework 4.5.1](http://www.microsoft.com/en-us/download/details.aspx?id=40773)

      * Install [Python 2.7](https://www.python.org/downloads/) (`v3.x.x` is not supported), and run `npm config set python python2.7` (or see below for further instructions on specifying the proper Python version and path.)
      * Launch cmd, `npm config set msvs_version 2015`

    If the above steps didn't work for you, please visit [Microsoft's Node.js Guidelines for Windows](https://github.com/Microsoft/nodejs-guidelines/blob/master/windows-environment.md#compiling-native-addon-modules) for additional tips.

* `npm install -g gulp`

### Ubuntu
* `sudo apt-get install g++`
* `sudo apt-get install git`
* `curl -sL https://deb.nodesource.com/setup_5.x | sudo -E bash -`
* Others:
    * `python` (`v2.7` recommended, `v3.x.x` is __*not*__ supported)
    * `make`
    * A proper C/C++ compiler toolchain, like [GCC](https://gcc.gnu.org)
* `sudo apt-get install nodejs`
* `sudo apt-get install npm`
* `sudo npm install -g gulp node-gyp node-sass`

## Setup

 * run `> sudo npm install` (Manchmal treten Fehler beim ersten durchlauf auf. In diesem Fall einfach nochmal Ausführen)
 * run `> gulp`
