var path = require('path');

var projectDir = path.join(__dirname, '/..');

browserSync = require('browser-sync').create();

/*
 * Configuration Options for the Gulp Build
 */
module.exports = {

    /*
     * Project Structure
     */
    project: {
        base: projectDir,
        app: {
            base: path.join(projectDir, '/app'),
            js:'/scripts',
            main: 'index.js'
        },
        css: {
            base: path.join(projectDir, '/app/styles'),
            main: 'main.scss'
        },
        assets: {
            img: path.join(projectDir, '/app/img'),
            font: path.join(projectDir, '/app/fonts'),
            bootstrap: path.join(projectDir, '/node_modules/bootstrap-sass')
        },
        target: {
            base: path.join(projectDir, '/target'),
            main: 'app.js',
            css: '/styles',
            img: '/img',
            font: '/fonts',
            bootstrap: {
                font: '/fonts'
            }
        }
    },

    /*
     * Connect HTTP-Server Options
     */
    server: {
        restUrl: 'http://localhost:8081',
        port: 80,
        reloadDelay: 1000
    }
};