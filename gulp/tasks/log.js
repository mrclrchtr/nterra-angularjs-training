var gulp = require('gulp'),
    util = require('gulp-util'),
    log = util.log,
    colors = util.colors;

var exports = module.exports = {};

exports.error = function(err) {
    log(colors.red.bold('[ERROR] '),
        colors.red.bold(err.type + " " + err.name + ":"),
        colors.white(err.message));
};

exports.warn = function(warn) {
    log(colors.orange.bold('[WARN]  '), colors.white(warn));
};

exports.info = function(info) {
    log(colors.blue.bold('[INFO]  '), colors.white(info));
};

exports.debug = function(debug) {
    log(colors.green.bold('[DEBUG] '), colors.white(debug));
};