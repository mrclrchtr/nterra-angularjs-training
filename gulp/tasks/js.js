var gulp = require('gulp'),
    gutil = require('gulp-util'),
    path = require('path'),
    sourcemaps = require('gulp-sourcemaps'),
    browserify = require('browserify'),
    watchify = require('watchify'),
    source = require('vinyl-source-stream'),
    buffer = require('vinyl-buffer');

var cfg = require('../config');
var log = require('./log');

function bundleScripts(opts) {
    var _browserify = null,
        _browserifyOpts = {
            entries: path.join(path.join(cfg.project.app.base, cfg.project.app.js), cfg.project.app.main),
            debug: true,
            cache: {}, // required by <watchify>, DO NOT CHANGE !!!
            packageCache: {}, // required by <watchify>, DO NOT CHANGE !!!
            fullPaths: true // required by <watchify>, DO NOT CHANGE !!!
        };

    if (opts.watch) {
        _browserify = watchify(browserify(_browserifyOpts))
            .transform("babelify", {presets: ["es2015"]})
            .on('update', _bundle)
            .on('log', gutil.log);
    }
    else {
        _browserify = browserify(_browserifyOpts)
            .transform("babelify", {presets: ["es2015"]});
    }

    function _bundle() {
        return _browserify.bundle()
            .on('error', gutil.log.bind(gutil, 'Browserify Error'))
            .pipe(source(cfg.project.target.main))
            .pipe(buffer())
            .pipe(sourcemaps.init({loadMaps: true}))
            .pipe(sourcemaps.write('./'))
            .pipe(gulp.dest(cfg.project.target.base))
            .pipe(browserSync.reload({ stream: true, once: true }));
    }

    return _bundle;
}

gulp.task('compile-dev', bundleScripts({watch: false}));
gulp.task('compile-dev-watch', bundleScripts({watch: true}));