var gulp = require('gulp'),
    path = require('path');

var cfg = require('../config');

function copyAssets() {
    gulp.src(path.join(cfg.project.assets.font, '/**/*'))
        .pipe(gulp.dest(path.join(cfg.project.target.base, cfg.project.target.font)));
    gulp.src(path.join(cfg.project.assets.img, '/**/*'))
        .pipe(gulp.dest(path.join(cfg.project.target.base, cfg.project.target.img)));
    gulp.src(path.join(cfg.project.assets.bootstrap, '/assets/fonts/bootstrap/*.{eot,svg,ttf,woff,woff2}'))
        .pipe(gulp.dest(path.join(cfg.project.target.base, cfg.project.target.bootstrap.font)));
}

gulp.task('copy-assets-dev', copyAssets);
gulp.task('copy-assets-dev-watch', ['copy-assets-dev'], function () {
    gulp.watch(path.join(cfg.project.assets.font, '/**/*'), ['copy-assets-dev']);
    gulp.watch(path.join(cfg.project.assets.img, '/**/*'), ['copy-assets-dev']);
});
gulp.task('copy-assets-dev-reload', ['copy-assets-dev'], function() {
    browserSync.reload();
});
gulp.task('copy-assets-dev-watch-reload',['copy-assets-dev'], function () {
    gulp.watch(path.join(cfg.project.assets.font, '/**/*'), ['copy-assets-dev-reload']);
    gulp.watch(path.join(cfg.project.assets.img, '/**/*'), ['copy-assets-dev-reload']);
});