var gulp = require('gulp');

gulp.task('build-dev', ['clean-dev', 'compile-dev', 'copy-html-dev', 'copy-assets-dev', 'sass-dev']);
gulp.task('build-dev-watch', ['clean-dev', 'compile-dev-watch', 'copy-html-dev-watch', 'copy-assets-dev-watch', 'sass-dev-watch']);
gulp.task('build-dev-watch-reload', ['clean-dev', 'compile-dev-watch', 'copy-html-dev-watch-reload', 'copy-assets-dev-watch-reload', 'sass-dev-watch-reload']);
