'use strict';

var gulp = require('gulp'),
    path = require('path'),
    sourcemaps = require('gulp-sourcemaps'),
    sass = require('gulp-sass');

var cfg = require('../config');

function compileSass() {
    gulp.src(path.join(cfg.project.css.base, cfg.project.css.main))
        .pipe(sourcemaps.init())
        .pipe(sass().on('error', sass.logError))
        .pipe(sourcemaps.write())
        .pipe(gulp.dest(path.join(cfg.project.target.base, cfg.project.target.css)));
}

gulp.task('sass-dev', compileSass);
gulp.task('sass-dev-watch', ['sass-dev'], function () {
    gulp.watch(path.join(cfg.project.css.base, '/**/*.scss'), ['sass-dev']);
});
gulp.task('sass-dev-reload', ['sass-dev'], function() {
    browserSync.reload();
});
gulp.task('sass-dev-watch-reload',['sass-dev'], function () {
    gulp.watch(path.join(cfg.project.css.base, '/**/*.scss'), ['sass-dev-reload']);
});