var gulp = require('gulp'),
    cfg = require('../config'),
    path = require('path'),
    url = require('url'),
    proxyMiddleware = require('proxy-middleware'),
    modRewrite = require('connect-modrewrite');

function serve() {

    var proxyOptions = url.parse(cfg.server.restUrl);
    proxyOptions.route = '/rest';

    browserSync.init({
        port: cfg.server.port,
        server: {
            baseDir: cfg.project.target.base,
            middleware: [
                proxyMiddleware(proxyOptions),
                modRewrite(['!\.html|\.js|\.woff2|\.ttf|\.woff|\.css|\.svg|\.png$ /index.html [L]'])
            ]
        },
        ghostMode: false,
        reloadDelay: cfg.server.reloadDelay
    });

}

gulp.task('serve-dev', serve);
gulp.task('serve-dev-watch', ['build-dev-watch'], serve);
gulp.task('serve-dev-watch-reload', ['build-dev-watch-reload'], serve);

gulp.task('default', ['serve-dev-watch-reload']);