var gulp = require('gulp'),
    path = require('path');

var cfg = require('../config');

function copyHtml() {
    gulp.src(path.join(cfg.project.app.base, '/**/*.html'))
        .pipe(gulp.dest(cfg.project.target.base));
}

gulp.task('copy-html-dev', copyHtml);
gulp.task('copy-html-dev-watch', ['copy-html-dev'], function () {
    gulp.watch(path.join(cfg.project.app.base, '/**/*.html'), ['copy-html-dev']);
});
gulp.task('copy-html-dev-reload', ['copy-html-dev'], function() {
    browserSync.reload();
});
gulp.task('copy-html-dev-watch-reload',['copy-html-dev'], function () {
    gulp.watch(path.join(cfg.project.app.base, '/**/*.html'), ['copy-html-dev-reload']);
});