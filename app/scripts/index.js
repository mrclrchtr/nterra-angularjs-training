'use strict';

import 'angular';
import 'angular-animate';
import 'angular-aria';
import 'angular-touch';
import 'angular-ui-router';

import 'angular-ui-bootstrap';
import 'angular-websocket';

import 'lodash';
import 'angularjs-toaster';

import './config';

// Controller
import './controller/ChatCtrl';
import './controller/NavbarCtrl';
import './controller/ProfileCtrl';
import './controller/RegistrationCtrl';

// Services
import './services/ChatAPI';
import './services/MessageService';
import './services/Profile';