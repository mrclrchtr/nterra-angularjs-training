'use strict';

import {appModule} from './app';

appModule.value('clientConfig', {
    server: "localhost:8080"
});