import angular from 'angular';

export var appModule = angular.module('appModule', [
    'ngAnimate',
    'ui.router',
    'ui.bootstrap',
    'ngTouch',
    'ngWebSocket',
    'toaster'
]);

appModule.config([
    '$locationProvider', '$stateProvider', '$urlRouterProvider',
    ($locationProvider, $stateProvider, $urlRouterProvider) => {

        $locationProvider.html5Mode(true);

        // if none of the above states are matched, use this as the fallback
        $urlRouterProvider.otherwise('/registration');

        $stateProvider
            .state('root', {
                url: "",
                views: {
                    'layout': {
                        templateUrl: "templates/layout/layout.html"
                    },
                    'navbar@root': {
                        templateUrl: "templates/layout/navbar.html",
                        controller: 'NavbarCtrl'
                    }
                }
            })
            .state('root.chat', {
                url: '/chat/:name',
                views: {
                    'content': {
                        templateUrl: '/templates/views/chat.html',
                        controller: 'ChatCtrl'
                    }
                }
            })
            .state('root.profile', {
                url: '/profile',
                views: {
                    'content': {
                        templateUrl: '/templates/views/profile.html',
                        controller: 'ProfileCtrl'
                    }
                }
            })
            .state('root.registration', {
                url: '/registration',
                views: {
                    'content': {
                        templateUrl: '/templates/views/registration.html',
                        controller: 'RegistrationCtrl'
                    }
                }
            });
    }]);