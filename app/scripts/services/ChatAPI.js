'use strict';

import {appModule} from '../app';

// Stellt die Kommunikation zwischen dem Rest-Server und der Applikation her
class ChatAPI {

    constructor($q, $http, $log) {
        let self = this;
        self.$q = $q;
        self.$http = $http;
        self.$log = $log;
    }

    getUser(username) {

        let self = this,
            deferred = self.$q.defer(),
            req = self.$http.get('/rest/users/' + username); // Create a request-object to retrieve the user from /rest/users/ with the parameter 'username'

        req.success((data, status) => { // If successfully
            self.$log.info('/rest/users/' + username + ' succeeded', data, status); // Write a log message
            deferred.resolve(data); // and send the data to the promise
        });

        req.error((data, status) => { // If not successfully
            self.$log.error('/rest/users/' + username + ' failed', data, status); // Write a log message
            deferred.reject(data); // and send the not successfully as a reject
        });

        return deferred.promise; // returns a promise, that the there will be an answer anytime

    }

    createUser(username, email) {

        let self = this,
            deferred = self.$q.defer(),
            data = {
                "username": username,
                "email": email
            },
            req = self.$http.post('/rest/users', data); // Create a request-object to post the user to /rest/users with the data parameter 'username' and 'email'

        req.success(function (data, status, headers, config) { // If successfully
            console.log('success - status: ', status, data); // Write a log message
            deferred.resolve(data); // and send the data to the promise
        });

        req.error(function (data, status) { // If not successfully
            console.log('error - status: ', status, data); // Write a log message
            deferred.reject(data); // and send the not successfully as a reject
        });

        return deferred.promise; // returns a promise, that the there will be an answer anytime

    }

    updateUser(username, email) {

        let self = this,
            deferred = self.$q.defer(),
            data = {
                "username": username,
                "email": email
            },
            req = self.$http.put('/rest/users' + username, data); // Create a request-object to update the user to /rest/users with the data parameter 'username' and 'email'

        req.success(function (data, status, headers, config) { // If successfully
            console.log('success - status: ', status, data); // Write a log message
            deferred.resolve(data); // and send the data to the promise
        });

        req.error(function (data, status) { // If not successfully
            console.log('error - status: ', status, data); // Write a log message
            deferred.reject(data); // and send the not successfully as a reject
        });

        return deferred.promise; // returns a promise, that the there will be an answer anytime

    }

    getChannelList() {

        let self = this,
            deferred = self.$q.defer(),
            req = self.$http.get('/rest/channels'); // Create a request-object to retrieve the channels from /rest/channels

        req.success((data, status) => { // If successfully
            self.$log.info('/rest/channels succeeded', data, status); // Write a log message
            deferred.resolve(data); // and send the data to the promise
        });

        req.error((data, status) => { // If not successfully
            self.$log.error('/rest/channels failed', data, status); // Write a log message
            deferred.reject(data); // and send the not successfully as a reject
        });

        return deferred.promise; // returns a promise, that the there will be an answer anytime

    }

    getChannelList() {

        let self = this,
            deferred = self.$q.defer(),
            req = self.$http.get('/rest/channels');

        req.success((data, status) => {
            self.$log.info('/rest/channels succeeded', data, status);
            deferred.resolve(data);
        });

        req.error((data, status) => {
            self.$log.error('/rest/channels failed', data, status);
            deferred.reject(data);
        });

        return deferred.promise;

    }

    createChannel(name, topic) {

        let self = this,
            deferred = self.$q.defer(),
            data = {
                "name": name,
                "topic": topic
            },
            req = self.$http.post('/rest/channels', data);

        req.success(function (data, status, headers, config) {
            console.log('success - status: ', status, data);
            deferred.resolve(data);
        });

        req.error(function (data, status) {
            console.log('error - status: ', status, data);
            deferred.reject(data);
        });

        return deferred.promise;

    }

}

appModule.service('ChatAPI', ['$q', '$http', '$log', ChatAPI]);

