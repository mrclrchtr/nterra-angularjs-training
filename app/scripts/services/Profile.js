'use strict';

import {appModule} from '../app';

// Service um das Profil über mehrere Controller hinweg zu speichern
appModule.factory('Profile', [
    function () {
        return {
            username: "",
            email: ""
        };
    }]);