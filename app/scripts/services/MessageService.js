'use strict';

import {appModule} from '../app';

// Stellt die Verbindung zum WebSocket-Server her
appModule.service('MessageService', ['$log', '$websocket', 'clientConfig',
    function($log, $websocket, clientConfig){
 
    this.collection = []; // Message collection
    this.dataStream = null;
        
    this.currentChannel = ""; // Für log (onOpen und onClose)

    this.connect = function (channel) {

        let self = this;

        this.currentChannel = channel;

        if (this.dataStream) {
            this.dataStream.close();
        }

        this.dataStream = $websocket('ws://'+ clientConfig.server + '/channel/' + channel);

        // Wenn eine Nachricht ankommt, wird sie in der collection gespeichert
        this.dataStream.onMessage(function (message) {
            self.collection.push(JSON.parse(message.data));
            $log.debug('Message received: ' + message.data);
        });

        this.dataStream.onOpen(function () {
            $log.debug("connected to " + self.currentChannel);
        });

        // Nach dem Schließen wird die collection geleert
        this.dataStream.onClose(function () {
            $log.debug("disconnected from " + self.currentChannel);
            self.collection.length = 0; // -> Delete collection
        });
    };

    // Eine Nachricht an den Server senden
    this.send = function (sender, message) {
        this.dataStream.send(JSON.stringify({
            sender: sender,
            message: message
        }));
    }

}]);