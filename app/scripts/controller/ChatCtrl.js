'use strict';

import {appModule} from '../app';

appModule.controller('ChatCtrl', [
    '$scope', '$log', '$stateParams', 'MessageService', 'Profile',
    function ($scope, $log, $stateParams, MessageService, Profile) {

        $scope.channelName = $stateParams.name; // set the channel name

        $scope.input = ""; // clean up input

        $scope.MessageService = MessageService;
        $scope.MessageService.connect($scope.channelName); // connect to the websocket with channelName

        $scope.send = function(){
            $scope.MessageService.send(Profile.username, $scope.input); // sends the input to the websocket
            $scope.input = ""; // clean up input
        }
        
    }
]);