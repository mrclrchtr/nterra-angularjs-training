'use strict';

import {appModule} from '../app';

appModule.controller('NavbarCtrl', [
    '$scope', '$log', '$timeout', '$uibModal', 'ChatAPI', 'Profile',
    function ($scope, $log, $timeout, $uibModal, ChatAPI, Profile) {

        $scope.channelList = [];
        $scope.profile = Profile;

        $scope.fillChannelList = function () {

            var req = ChatAPI.getChannelList();

            req.then(function (data) {
                $scope.channelList = data.channels;
                $log.debug("channelList loaded");
            });

            req.catch(function (error) {
                $log.error(error);
            });

        };
        // initial filling
        $scope.fillChannelList();

        $scope.openCreateChannelModal = function () {
            var modalInstance = $uibModal.open({
                templateUrl: '/templates/modals/create-channel-modal.html',
                controller: ['$scope', '$uibModalInstance', 'ChatAPI', ($modalScope, $uibModalInstance, ChatAPI) => {

                    $modalScope.name = "";
                    $modalScope.topic = "";

                    $modalScope.ok = function () {
                        ChatAPI.createChannel($modalScope.name, $modalScope.topic);
                        $uibModalInstance.close();
                    };

                    $modalScope.cancel = function () {
                        $uibModalInstance.dismiss('cancel');
                    };
                }]
            });

            modalInstance.result.then(function () {
                // time to wait before refill channelList
                $timeout(function () {
                    $scope.fillChannelList()
                }, 200);
            });
        };

    }
]);