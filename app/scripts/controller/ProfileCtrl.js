'use strict';

import {appModule} from '../app';

appModule.controller('ProfileCtrl', [
    '$scope', '$log', 'Profile',
    function ($scope, $log, Profile) {
        $scope.profile = Profile;
    }
]);