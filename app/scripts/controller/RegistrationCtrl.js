'use strict';

import {appModule} from '../app';

appModule.controller('RegistrationCtrl', [
    '$scope', '$log', '$state', 'ChatAPI', 'Profile', 'toaster',
    function ($scope, $log, $state, ChatAPI, Profile, toaster) {

        $scope.profile = Profile;

        $scope.data = {
            login: {
                username: ""
            },
            registration: {
                username: "",
                email: ""
            }
        };

        $scope.login = function () {
            var req = ChatAPI.getUser($scope.data.login.username); // create the request object (promise)

            req.then(function (data) {
                if (data.success == true) {  // if the promise returns successfully
                    $scope.profile.username = data.user.username; // set the vars
                    $scope.profile.email = data.user.email;
                    $log.debug("User loaded: " + data); // write a log message
                    $state.go('root.profile') // go to the profile page
                } else { // if the promise returns not successfully
                    toaster.pop('error', "ERROR", "User not found!"); // show a popup message
                }
            });

            req.catch(function (error) { // on error
                $log.error(error); // write a log message
                toaster.pop('error', "ERROR", error); // show a popup message
            });
        };

        $scope.register = function () {
            var req = ChatAPI.createUser($scope.data.registration.username, $scope.data.registration.email); // create the request object (promise)

            req.then(function (data) { // if the promise returns successfully
                if (data.success == true) {
                    $scope.profile.username = data.user.username;// set the vars
                    $scope.profile.email = data.user.email;
                    $log.debug("User registered: " + data);// write a log message
                    $state.go('root.profile')
                } else {// if the promise returns not successfully
                    toaster.pop('error', "ERROR", "Cannot create user!");// show a popup message
                }
            });

            req.catch(function (error) {
                $log.error(error);// write a log message
                toaster.pop('error', "ERROR", error);// show a popup message
            });
        };

    }
]);